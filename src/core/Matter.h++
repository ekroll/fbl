#ifndef MATTER_H
#define MATTER_H


#include "matter/Mesh.h++"
#include "matter/Shader.h++"
#include "Camera.h++"


class Matter {
    public:
        Matter(Mesh* mesh, Shader* shader) {
            m_mesh = mesh;
            m_shader = shader;
        }

        void progress(Camera* camera);

        ~Matter();
    protected:
    private:
        Mesh* m_mesh;
        Shader* m_shader;
};


#endif
