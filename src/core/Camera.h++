#ifndef FRAME_H
#define FRAME_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <stdexcept>
#include <iostream>
#include <string>

class Camera  {
    public:
        Camera(
            const std::string& title,
            int width, 
            int height, 
            float fov,
            float near,
            float far
        );

        Camera() = default;

        bool is_on();
        void shutter_open();

        glm::mat4 get_view_matrix();
        glm::mat4 get_perspective_matrix();

        void capture_image();

        ~Camera();

    protected:
    private:
        GLFWwindow* m_window;
        glm::mat4 m_perspective;
        glm::vec3 m_location;
        glm::vec3 m_forward;
        glm::vec3 m_up;
};

#endif
