#ifndef SPACETIME_H
#define SPACETIME_H

#include "Matter.h++"
#include "Camera.h++"

class Spacetime {
    public:
        Spacetime(Camera* camera)
            : m_camera { std::move(camera) } {}

        void set_camera(Camera* camera);
        void add_matter(Matter* matter);
        void del_matter(int index);
        void progress();

        ~Spacetime();
    protected:
    private:
        std::vector<Matter*> m_matter;
        Camera* m_camera;
};

#endif
