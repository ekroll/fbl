#include "Spacetime.h++"


void Spacetime::set_camera(Camera* camera) {
    m_camera = camera;
}


void Spacetime::add_matter(Matter* matter) {
    m_matter.push_back(matter);
}


void Spacetime::del_matter(int index) {
    m_matter.erase(m_matter.begin() + index);
}


void Spacetime::progress() {
    for (int i = 0; i < m_matter.size(); i++) {
        m_matter[i]->progress(m_camera);
    }
}


Spacetime::~Spacetime() {
    std::cout << "Spacetime destructor" << std::endl;
}
