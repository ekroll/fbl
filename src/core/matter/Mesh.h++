#ifndef MESH_H
#define MESH_H

#include <vector>
#include <iostream>
#include <string> 
#include <fstream>
#include <sstream>
#include <algorithm>

#include <GL/glew.h>

#include "mesh/Vertex.h++"

class Mesh {
    public:
        Mesh(const std::string& file_name);
        Mesh(Vertex* vertices);
        Mesh() = default;

        void draw();

        ~Mesh();
    protected:
    private:
        std::vector<glm::vec3> m_vertex_positions;
        std::vector<glm::vec2> m_texture_coordinates;
        std::vector<glm::vec3> m_vertex_normals;

        std::vector<GLint> m_vertex_position_indices;
        std::vector<GLint> m_texture_coordinate_indices;
        std::vector<GLint> m_vertex_normal_indices;

        GLuint m_vertex_array_id;
        GLuint m_vertex_buffer;
        unsigned int m_vertex_count;

        void upload_mesh_data(Vertex* vertices);
};

#endif
