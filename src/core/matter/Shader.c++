#include "Shader.h++"

static GLuint create_shader(
    const std::string& text,
    GLenum shader_type
);

static GLuint create_shader(
    const std::string& text,
    GLenum shader_type
) {
    GLuint shader = glCreateShader(shader_type);

    if (shader == 0) {
        std::cerr << "Failed to create shader" << std::endl;
    }

    else {
        const GLchar* src_str[1];
        GLint src_str_len[1];

        src_str[0] = text.c_str();
        src_str_len[0] = text.length();

        glShaderSource(shader, 1, src_str, src_str_len);

        std::cout << "Compiling shader of type " << shader_type << std::endl;
        glCompileShader(shader);
    }

    return shader;
}


Shader::Shader (
    const std::string& vs_src, 
    const std::string& fs_src
) {
    std::cout << "Shader constructor" << std::endl;

    m_program = glCreateProgram();

    m_vertex_shader = create_shader(
        vs_src,
        GL_VERTEX_SHADER
    );

    m_fragment_shader = create_shader(
        fs_src,
        GL_FRAGMENT_SHADER
    );

    glAttachShader(m_program, m_vertex_shader);
    glAttachShader(m_program, m_fragment_shader);

    glBindAttribLocation(m_program, 0, "a_vertex_position");
    glBindAttribLocation(m_program, 1, "a_texture_coordinate");

    glLinkProgram(m_program);
    glValidateProgram(m_program);

    m_uniforms[U_MODEL_MATRIX] = glGetUniformLocation(m_program, "u_model_matrix");
    m_uniforms[U_VIEW_MATRIX] = glGetUniformLocation(m_program, "u_view_matrix");
    m_uniforms[U_PERSPECTIVE_MATRIX] = glGetUniformLocation(m_program, "u_perspective_matrix");

    std::cout << "Shader constructor done" << std::endl;
}


void Shader::bind() {
    glUseProgram(m_program);
    m_is_bound = true;
    std::cout << "Bound shader program " << m_program << std::endl;
}


void Shader::set_model_matrix(glm::mat4 matrix) {
    if (m_is_bound) {
        glUniformMatrix4fv(
            m_uniforms[U_MODEL_MATRIX], 
            1, 
            GL_FALSE, 
            &matrix[0][0]
        );
    }

    else {
        std::cout << "Cannot set uniform (shader not bound)" << std::endl;
    }
}


void Shader::set_view_matrix(glm::mat4 matrix) {
    if (m_is_bound) {
        glUniformMatrix4fv(
            m_uniforms[U_MODEL_MATRIX], 
            1, 
            GL_FALSE, 
            &matrix[0][0]
        );
    }
}


void Shader::set_perspective_matrix(glm::mat4 matrix) {
    if (m_is_bound) {
        glUniformMatrix4fv(
            m_uniforms[U_PERSPECTIVE_MATRIX],
            1,
            GL_FALSE,
            &matrix[0][0]
        );
    }
}


void Shader::unbind() {
    glUseProgram(0);
    m_is_bound = false;
}


Shader::~Shader() {
    std::cout << "Shader destructor" << std::endl;
    glDetachShader(m_program, m_fragment_shader);
    glDeleteShader(m_fragment_shader);

    /*
    glDetachShader(m_program, m_vertex_shader]);
    glDeleteShader(m_vertex_shader);
    */

    glDetachShader(m_program, m_vertex_shader);
    glDeleteShader(m_vertex_shader);
    glDeleteProgram(m_program);
}






