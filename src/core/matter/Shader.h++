#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include <iostream>

#include "shader/Transform.h++"
#include "shader/Texture.h++"

class Shader {
    public:
        Shader(const std::string& vs_src, const std::string& fs_src);
        Shader() = default;

        void bind();

        void set_model_matrix(glm::mat4 matrix);
        void set_view_matrix(glm::mat4 matrix);
        void set_perspective_matrix(glm::mat4 matrix);

        void unbind();

        ~Shader();

    protected:
    private:
        static const unsigned int NUM_SHADERS = 2;

        enum {
            U_MODEL_MATRIX,
            U_VIEW_MATRIX,
            U_PERSPECTIVE_MATRIX,
            NUM_UNIFORMS
        };

        GLuint m_uniforms[NUM_UNIFORMS];
        GLuint m_program;
        GLuint m_vertex_shader;
        GLuint m_fragment_shader;

        bool m_is_bound = false;
};

#endif
