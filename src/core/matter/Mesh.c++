#include "Mesh.h++"

void Mesh::upload_mesh_data(Vertex* vertices) {
    m_vertex_count = sizeof(vertices)/sizeof(vertices[0]);

    glGenVertexArrays(1, &m_vertex_array_id);
    glBindVertexArray(m_vertex_array_id);

    glGenBuffers(1, &m_vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertex_buffer);
    glBufferData(
        GL_ARRAY_BUFFER,
        m_vertex_count * sizeof(vertices[0]),
        vertices,
        GL_STATIC_DRAW
    );

    int num_components = 0;
    GLintptr element_offset = 0;

    enum {
        POS_ATTRIB,
        NORMAL_ATTRIB,
        TEXCOORD_ATTRIB
    };

    for (int i = 0; i < 3; i++) {
        switch (i) {
            case POS_ATTRIB:
                num_components = 3;
                break;

            case NORMAL_ATTRIB:
                num_components = 3;
                break;

            case TEXCOORD_ATTRIB:
                num_components = 2;
                break;
        }

        glEnableVertexAttribArray(i);
        glVertexAttribPointer(
            i,                          // Attrib index... layout (location = i)
            num_components,       // Number of attribute components
            GL_FLOAT,                   // Component type
            GL_FALSE,                   // Normalize?
            sizeof(vertices[0]),        // Single attrib size
            (GLvoid*) element_offset    // Component offset from start of attrib
        );

        std::cout << "Calculating attribute " << i << " pointer offset" << std::endl;
        element_offset += (GLintptr) sizeof(float) * num_components;
    }

    std::cout << "Unbinding vertex array" << std::endl;
    glBindVertexArray(0);

    std::cout << "Mesh constructor done" << std::endl;
}


Mesh::Mesh(const std::string& file_name) {
    std::cout << "Loading obj file " << file_name << std::endl;
    std::ifstream in_file(file_name);

    if (!in_file.is_open()){
        throw std::runtime_error(
            "Could not open obj file"
        );
    }

    std::string line = "";
    std::stringstream ss;
    std::string prefix = "";

    glm::vec3 temp_vec3;
    glm::vec2 temp_vec2;

    std::vector<glm::vec3> positions;
    std::vector<glm::vec2> texture_coordinates;
    std::vector<glm::vec3> normals;

    while (std::getline(in_file, line)) {
        ss.clear();
        ss.str(line);
        ss >> prefix;

        if (prefix == "v") {
                ss >> temp_vec3.x >> temp_vec3.y >> temp_vec3.z;
                positions.push_back(temp_vec3);
        }

        else if (prefix == "vt") {
                ss >> temp_vec2.x >> temp_vec2.y;
                texture_coordinates.push_back(temp_vec3);
        }

        else if (prefix == "vn") {
                ss >> temp_vec3.x >> temp_vec3.y >> temp_vec3.z;
                normals.push_back(temp_vec3);
        }

        else if (prefix == "f") {
        }
    }
}


Mesh::Mesh(Vertex* vertices) {
    std::cout << "Mesh constructor" << std::endl;
    upload_mesh_data(
        vertices
    );
}


void Mesh::draw() {
    glBindVertexArray(m_vertex_array_id);
    glDrawArrays(GL_TRIANGLES, 0, m_vertex_count);
    //glDisableVertexAttribArray(0);
    glBindVertexArray(0);
    std::cout << "Drew vertex array object " << m_vertex_array_id<< std::endl;
}


Mesh::~Mesh() {
    std::cout << "Mesh destructor" << std::endl;
    glDeleteVertexArrays(1, &m_vertex_array_id);
}
