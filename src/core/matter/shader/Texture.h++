#ifndef TEXTURE_H
#define TEXTURE_H

#include <string>
#include <GL/glew.h>
#include <cassert>
#include <iostream>
#include "texture/stb_image.h"

class Texture {
    public:
        Texture(const std::string& file_name);

        void occupy_slot(unsigned int unit);

        ~Texture();
    protected:
    private:
        GLuint m_texture;
};

#endif
