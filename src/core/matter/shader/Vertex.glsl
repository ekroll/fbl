#version 330 core

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec2 a_texture_coordinate;

uniform mat4 u_model_matrix;
uniform mat4 u_view_matrix;
uniform mat4 u_perspective_matrix;

out vec2 v_texture_coordinate;

void main() {
    gl_Position = 
        u_perspective_matrix 
      * u_view_matrix 
      * u_model_matrix 
      * vec4(a_position, 1.0)
    ;

    v_texture_coordinate = a_texture_coordinate;
}

