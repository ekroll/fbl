#include "Transform.h++"


Transform::Transform(
    const glm::vec3& loc = glm::vec3(),
    const glm::vec3& rot = glm::vec3(),
    const glm::vec3& scale = glm::vec3(1.0f, 1.0f, 1.0f)
) {
    std::cout << "Transform constructor" << std::endl;

    m_scale = scale;
    std::cout << "stored scale " << scale.x << "," << scale.y << "," << scale.z << std::endl;

    m_rot = rot;
    std::cout << "stored rotation " << rot.x << "," << rot.y << "," << rot.z << std::endl;

    m_loc = loc;
    std::cout << "stored location " << loc.x << "," << loc.y << "," << loc.z << std::endl;
}

glm::mat4 Transform::get_model_matrix() {
    glm::mat4 scale_matrix = glm::scale(m_scale);

    glm::mat4 x_rot_matrix = glm::rotate(m_rot.x, glm::vec3(1,0,0));
    glm::mat4 y_rot_matrix = glm::rotate(m_rot.y, glm::vec3(0,1,0));
    glm::mat4 z_rot_matrix = glm::rotate(m_rot.z, glm::vec3(0,0,1));
    glm::mat4 rot_matrix = z_rot_matrix * y_rot_matrix * x_rot_matrix;

    glm::mat4 loc_matrix = glm::translate(m_loc);

    glm::mat4 model_matrix = loc_matrix * rot_matrix * scale_matrix;
    
    return model_matrix;
}


Transform::~Transform() {
    std::cout << "Transform destructor" << std::endl;
}
