#ifndef TRANSFORM_H
#define TRANSFORM_H


#include <iostream>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>


class Transform {
    public:
        Transform() = default;

        Transform(
            const glm::vec3& loc,
            const glm::vec3& rot,
            const glm::vec3& scale
        );


        inline glm::vec3 get_loc() { return m_loc; }
        inline glm::vec3 get_rot() { return m_rot; }
        inline glm::vec3 get_scale() { return m_scale; }

        glm::mat4 get_model_matrix();

        ~Transform();

    protected:

    private:
        glm::vec3 m_scale = glm::vec3(1.0, 1.0, 1.0);
        glm::vec3 m_rot = glm::vec3(0, 0, 0);
        glm::vec3 m_loc = glm::vec3(0, 0, 0);
};

#endif
