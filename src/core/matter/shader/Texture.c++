#include "Texture.h++"

Texture::Texture(const std::string& file_name) {
    std::cout << "Texture constructor" << std::endl;

    int width, height, components;
    stbi_set_flip_vertically_on_load(1);

    unsigned char* image_data = stbi_load(
        (file_name).c_str(),
        &width,
        &height,
        &components,
        4
    );

    glGenTextures(1, &m_texture);
    glBindTexture(GL_TEXTURE_2D, m_texture);

    // Texture repetition
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

    // Texture resize-filtering
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // No mercy for small textures

    glTexImage2D(
        GL_TEXTURE_2D,
        0, // Mipmap level
        GL_RGBA8, // Format stored on the GPU
        width,
        height,
        0, // Border?
        GL_RGBA, // Format from the image loader
        GL_UNSIGNED_BYTE,
        image_data // Image data in the format on the line above
    );

    glBindTexture(GL_TEXTURE_2D, 0);

    if (image_data == NULL)  {
        std::cerr << "Failed to load texture from " << file_name << std::endl;
    }

    else {
        std::cout << "Image data loaded from " << file_name << std::endl;
        stbi_image_free(image_data);
    }
    
    std::cout << "Texture constructor done" << std::endl;
}

void Texture::occupy_slot(unsigned int unit = 0) {
    assert(unit >= 0 && unit <= 31);
    glActiveTexture(GL_TEXTURE0 + unit);
    glBindTexture(GL_TEXTURE_2D, m_texture);
    std::cout << "Bound texture to slot " << unit << std::endl;
}

Texture::~Texture() {
    std::cout << "Texture destructor" << std::endl;
    glDeleteTextures(1, &m_texture);
}

