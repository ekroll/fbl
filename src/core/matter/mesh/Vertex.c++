#include "Vertex.h++"


Vertex::Vertex(glm::vec3 position, glm::vec2 texture_coordinate, glm::vec3 normal) {
    std::cout << "Vertex constructor" << std::endl;
    m_position = position;
    m_texture_coordinate = texture_coordinate;
    m_normal = normal;
}


Vertex::~Vertex() {
    std::cout << "Vertex destructor" << std::endl;
}
