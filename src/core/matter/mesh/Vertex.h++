#ifndef VERTEX_H
#define VERTEX_H

#include <iostream>
#include <glm/glm.hpp>

class Vertex {
    public:
        Vertex(
            const glm::vec3 position,
            const glm::vec2 texture_coordinate,
            const glm::vec3 normal
        );

        ~Vertex();

    protected:
    private:
        glm::vec3 m_position;
        glm::vec2 m_texture_coordinate;
        glm::vec3 m_normal;
};

#endif
