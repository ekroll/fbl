#include "Matter.h++"


void Matter::progress(Camera* camera) {
    m_shader->bind();
    Transform transform(
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 0.0f),
        glm::vec3(1.0f, 1.0f, 1.0f)
    );
    m_shader->set_model_matrix(transform.get_model_matrix());

    m_shader->set_view_matrix(
        camera->get_view_matrix()
    );

    m_shader->set_perspective_matrix(
        camera->get_perspective_matrix()
    );

    m_shader->unbind();
    m_mesh->draw();
}


Matter::~Matter() {
    std::cout << "Matter destructor" << std::endl;
}
