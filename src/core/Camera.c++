#include "Camera.h++"


void glDebugOutput(
    GLenum source,
    GLenum type,
    GLuint id,
    GLenum severity,
    GLsizei length,
    const GLchar *message,
    const void *userParam
) {
    if (
        id == 131169 || 
        id == 131185 || 
        id == 131218 || 
        id == 131204
    ) {
        return;
    }

    std::cout << "OpenGL debug message:"; 

    std::cout << std::endl << "\tType: ";

    switch (type) {
        case GL_DEBUG_TYPE_ERROR:               std::cout << "error";                   break;
        case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: std::cout << "deprecated behaviour";    break;
        case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  std::cout << "undefined behaviour";     break; 
        case GL_DEBUG_TYPE_PORTABILITY:         std::cout << "portability";             break;
        case GL_DEBUG_TYPE_PERFORMANCE:         std::cout << "performance";             break;
        case GL_DEBUG_TYPE_MARKER:              std::cout << "marker";                  break;
        case GL_DEBUG_TYPE_PUSH_GROUP:          std::cout << "push group";              break;
        case GL_DEBUG_TYPE_POP_GROUP:           std::cout << "pop group";               break;
        case GL_DEBUG_TYPE_OTHER:               std::cout << "other";                   break;
    }

    std::cout << std::endl << "\tSerevity: ";

    switch (severity) {
        case GL_DEBUG_SEVERITY_NOTIFICATION:    std::cout << "notification";            break;
        case GL_DEBUG_SEVERITY_HIGH:            std::cout << "high";                    break;
        case GL_DEBUG_SEVERITY_MEDIUM:          std::cout << "medium";                  break;
        case GL_DEBUG_SEVERITY_LOW:             std::cout << "low";                     break;
    }

    std::cout << std::endl << "\tSource: ";

    switch (source) {
        case GL_DEBUG_SOURCE_API:               std::cout << "api";                     break;
        case GL_DEBUG_SOURCE_WINDOW_SYSTEM:     std::cout << "window system";           break;
        case GL_DEBUG_SOURCE_SHADER_COMPILER:   std::cout << "shader compiler";         break;
        case GL_DEBUG_SOURCE_THIRD_PARTY:       std::cout << "third-party";             break;
        case GL_DEBUG_SOURCE_APPLICATION:       std::cout << "application";             break;
        case GL_DEBUG_SOURCE_OTHER:             std::cout << "other";                   break;
    }

    std::cout << std::endl << "\tMessage: " << message;
    std::cout << std::endl << "\tId: " << id << std::endl;
}


void on_window_resized(GLFWwindow* window, int width, int height) {
    std::cout << "Window resized to " << width << "x" << height << std::endl;
}


void on_framebuffer_resized(GLFWwindow* window, int width, int height) {
    std::cout << "Framebuffer resized to " << width << "x" << height << std::endl;
    glViewport(0, 0, width, height);
}


void on_dpi_scaled(GLFWwindow* window, float xscale, float yscale) {
    std::cout << "DPI scaled to " << xscale << "x" << yscale << std::endl;
}


void on_key_input(GLFWwindow* window, int key, int scancode, int action, int mods) {
    std::cout << "Registered key " << scancode << " action " << action << std::endl;

    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
        std::cout << "Quitting application" << std::endl;
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
}


Camera::Camera(const std::string& title, int width, int height, float fov, float near, float far) {
    std::cout << "Camera constructor" << std::endl;
    std::cout << "Initializing GLFW" << std::endl;
    
    if (!glfwInit()) {
        throw std::runtime_error("Failed to glfwInit()");
    }

    std::cout << "Requesting OpenGL Core Profile" << std::endl;
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    std::cout << "Requesting OpenGL version 4.3" << std::endl;
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    std::cout << "Requesting GLFW debug context" << std::endl;
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GLFW_TRUE);

    std::cout << "Requesting OpenGL forward compatability" << std::endl;
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);   glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

    std::cout << "Requesting doublebuffering" << std::endl;
    glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);

    std::cout << "Requesting unlimited refresh rate" << std::endl;
    glfwWindowHint(GLFW_REFRESH_RATE, GLFW_DONT_CARE);

    std::cout << "Requesting 8-bit color" << std::endl;
    glfwWindowHint(GLFW_RED_BITS, 8);
    glfwWindowHint(GLFW_GREEN_BITS, 8);
    glfwWindowHint(GLFW_BLUE_BITS, 8);
    glfwWindowHint(GLFW_ALPHA_BITS, 8);
    glfwWindowHint(GLFW_DEPTH_BITS, 16);

    std::cout << "Creating window" << std::endl;
    m_window = glfwCreateWindow(width, height, title.c_str(), NULL, NULL);

    if (m_window == NULL) {
        glfwTerminate();
        throw std::runtime_error("Failed to create window");
    }

    std::cout << "Making OpenGL context current" << std::endl;
    glfwMakeContextCurrent(m_window);
    glewExperimental = true;

    std::cout << "Initializing GLEW" << std::endl;
    if (glewInit() != GLEW_OK) {
        throw std::runtime_error("Failed to glewInit()");
    }

    std::cout << "Validating OpenGL support" << std::endl;

    if (!GLEW_VERSION_4_3) {
        throw std::runtime_error("OpenGL 4.3 core not available");
    }

    else {
        std::cout << "Using OpenGL 4.3 core" << std::endl;
    }

    int flags; 
    glGetIntegerv(GL_CONTEXT_FLAGS, &flags);

    if (flags & GL_CONTEXT_FLAG_DEBUG_BIT) {
        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(glDebugOutput, nullptr);

        glDebugMessageControl(
            GL_DONT_CARE,
            GL_DONT_CARE,
            GL_DONT_CARE,
            0, nullptr, 
            GL_TRUE
        );
    }

    else {
        throw std::runtime_error("Failed to aquire debug context");
    }

    std::cout << "Registering window event callbacks" << std::endl;
    glfwSetWindowSizeCallback(m_window, on_window_resized);
    glfwSetFramebufferSizeCallback(m_window, on_framebuffer_resized);
    glfwSetWindowContentScaleCallback(m_window, on_dpi_scaled);
    glfwSetKeyCallback(m_window, on_key_input);

    std::cout << "Camera constructor done" << std::endl;

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);

    m_perspective = glm::perspective(fov, (float)height/(float)width, near, far);
    m_location = glm::vec3(0.0, 0.0, 0.0);
    m_forward = glm::vec3(0.0, 0.0, 1.0);
    m_up = glm::vec3(0.0, 1.0, 0.0);
}



bool Camera::is_on() {
    return !glfwWindowShouldClose(m_window);
}


void Camera::shutter_open() {
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    glClearColor(0.0, 0.0, 0.0, 1.0);
}


glm::mat4 Camera::get_perspective_matrix() {
    return m_perspective;
}


glm::mat4 Camera::get_view_matrix() {
    return glm::lookAt(m_location, m_location + m_forward, m_up);
}


void Camera::capture_image() {
    glfwSwapBuffers(m_window);       
    glfwPollEvents();
}


Camera::~Camera() {
    std::cout << "Camera destructor" << std::endl;
    glfwDestroyWindow(m_window);
    glfwTerminate();    
}
