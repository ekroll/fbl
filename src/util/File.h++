#ifndef FILE_H
#define FILE_H

#include <fstream>
#include <iostream>
#include <string>

class File {
    public:
        File(const std::string path)
            : m_path { std::move(path) }
            {}

        std::string to_string();

        ~File();
    protected:
    private:
        const std::string m_path;
};

#endif
