#include "File.h++"


//dawd

std::string File::to_string() {
    std::ifstream file;
    file.open(m_path.c_str());

    std::string output;
    std::string line;

    if (file.is_open()) {
        while (file.good()) {
            getline(file, line);
            output.append(line + "\n");
        }

        std::cout << "Read string to memory from " << m_path
            << std::endl;
    }

    else {
        std::cerr << "File " << m_path << " could not be opened " 
            << std::endl;
    }

    return output;
}


File::~File() {}
