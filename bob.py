#!/usr/bin/python


def die_on_error(command):
    error = subprocess.check_output(command, shell=True)

    if error == 0:
        return

    else:
        sys.exit(error)


def build(src):
    for file in src.rglob("**/*.c++"):
        if file.ext in filetypes:
            obj_file = file.parent / file.stem + ".o"

            if obj_file.is_file():
                if file_is_old:
                    command = [c, cflags, "-o", obj_file, "-c", src_file]
                    die_on_error(command)
                    
                else:
                    obj_files.append(file)
                        next;

    return obj_files


def link(files, result, library=False, dynamic=False):
    obj_files = reverse(files);

    for obj_file in obj_files:
        print(obj_file)

    print("\n");


    command = [c, cflags, "-o", result, obj_files, lflags]
    die_on_error(command)

    print("Built $target:");


def main(args):
    if not args.build == False:
        obj_files = build(args.build)

    if not args.link == False:
        link(obj_files, args.link)

src = "./src";
log = "./log";
obj = "./obj";
pkg = "./out";
target = "feedback";


lflags = ["-lglfw", "-lOpenGL", "-lGLEW", "-lm"];
assure_file_exists("$log/bob.log");


