# Feedback (Does not work yet)
This was initially a part of Social Overengineering, but decided to make stuff simpler by putting engine code into a separate project. The goal of this project is to become a general "engine" for interactive physics. Since archlinux is the initial target platform, the library will be available as `feedback-bin` in the AUR. 

## Development platform:
* [Git](https://git-scm.com/)
* [GitLab](https://gitlab.com)
* [OS Installer](https://gitlab.com/ekroll/os-installer) (Arch btw)

## Programming languages
* [C++](http://www.cplusplus.org/)
* [GLSL](https://www.khronos.org/opengl/wiki/Core_Language_(GLSL))
* [Perl](https://www.perl.org/) (build and test automation)

## Dependencies:
* [Perl](https://www.perl.org/)
* [CPAN](https://www.cpan.org/)
* [GCC](https://gcc.gnu.org/)
* [GDB](https://www.gnu.org/software/gdb/)
* [glm](https://glm.g-truc.net/0.9.9/)
* [GLFW](https://www.glfw.org/)
* [GLEW](http://glew.sourceforge.net/)
* [OpenAL](https://www.youtube.com/channel/UCcAlTqd9zID6aNX3TzwxJXg) (Not used yet)
* [OpenGL](https://www.khronos.org/opengl/)
* [stb_image](https://github.com/nothings/stb)


## Nice material
* [LearnOpenGL](https://learnopengl.com/)
* [ThinMatrix](https://www.youtube.com/user/ThinMatrix) on 
[OpenAL](https://www.youtube.com/watch?v=BR8KjNkYURk&list=PLRIWtICgwaX2VNpAFjAZdlQw2pA1-5kU8)
* [TheCherno](https://www.youtube.com/user/TheChernoProject) on 
[modern OpenGL](https://www.youtube.com/watch?v=W3gAzLwfIP0&list=PLlrATfBNZ98foTJPJ_Ev03o2oq3-GGOS2).
* [thebennybox](https://www.youtube.com/user/thebennybox/featured) on 
[modern OpenGL](https://www.youtube.com/watch?v=ftiKrP3gW3k&list=PLEETnX-uPtBXT9T-hD0Bj31DSnwio-ywh).
* [The Art of Code](https://www.youtube.com/channel/UCcAlTqd9zID6aNX3TzwxJXg) on
shader programming.


Elias Flåte Ekroll
